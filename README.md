# 💦 OpenClient 💦      
the first free, open-sourced, clearly-documented minetest Client Utility Mod (CUM)         
v1.1 for minetest 5.0.1       
        
I'm going to be taking a break from developing OpenClient. If you'd like to develop OpenClient contact me on Discord @ algorithm#6184

# features (so far, v1.1):
- fullbright (https://github.com/MinetestClients/OpenClient/commit/fcdf71d65dc531ba1effbaf20328bb345153cd4b)
- nofall (no fall damage) (https://github.com/MinetestClients/OpenClient/commit/00c15982323c8662769ecaa692c5047fc247ad0b)
- 3rd person camera view can go inside nodes - press fn+7 (https://github.com/MinetestClients/OpenClient/commit/db18ab4b13750c1f7d2951b44b00b72c929c1176)
- fly, noclip, wireframe, fast mode (https://github.com/MinetestClients/OpenClient/commit/fb1651b55ce379e0da37b33da91f9d9d3288e73b)

# installation:
Mac OS:      
There are 2 easy ways you can install the latest version of OpenClient. The first is through brew install: https://github.com/MinetestClients/brewinstall (look in your /usr/local/Cellar folder to find minetest after you install it)      
The second method is through OpenClient's releases: https://github.com/MinetestClients/OpenClient/releases/download/v1.1/OpenClient.dmg    

Linux:      
1) Install dependencies:   
       
*For Debian/Ubuntu users:*

    sudo apt install g++ make libc6-dev libirrlicht-dev cmake libbz2-dev libpng-dev libjpeg-dev libxxf86vm-dev libgl1-mesa-dev libsqlite3-dev libogg-dev libvorbis-dev libopenal-dev libcurl4-gnutls-dev libfreetype6-dev zlib1g-dev libgmp-dev libjsoncpp-dev

*For Fedora users:*

    sudo dnf install make automake gcc gcc-c++ kernel-devel cmake libcurl-devel openal-soft-devel libvorbis-devel libXxf86vm-devel libogg-devel freetype-devel mesa-libGL-devel zlib-devel jsoncpp-devel irrlicht-devel bzip2-libs gmp-devel sqlite-devel luajit-devel leveldb-devel ncurses-devel doxygen spatialindex-devel bzip2-devel
    
*For Arch users:*

    sudo pacman -S base-devel libcurl-gnutls cmake libxxf86vm irrlicht libpng sqlite libogg libvorbis openal freetype2 jsoncpp gmp luajit leveldb ncurses

*For Alpine users:*

    sudo apk add build-base irrlicht-dev cmake bzip2-dev libpng-dev jpeg-dev libxxf86vm-dev mesa-dev sqlite-dev libogg-dev libvorbis-dev openal-soft-dev curl-dev freetype-dev zlib-dev gmp-dev jsoncpp-dev luajit-dev

2) Download code (OpenClient & minetest_game)    
```
wget https://github.com/MinetestClients/OpenClient/archive/v1.1.tar.gz
tar xf OpenClient-1.1.tar.gz
cd OpenClient-1.1
cd games/
wget https://github.com/minetest/minetest_game/archive/5.0.1.tar.gz
tar xf minetest_game-5.0.1.tar.gz
cd ..
```

3) Build:          
```
cmake . -DRUN_IN_PLACE=TRUE
make -j$(nproc)
```

4) Run:          
`./bin/minetest`

Windows:
1) Download & Install the following:
- [CMake](https://cmake.org/download/)
- [vcpkg](https://github.com/Microsoft/vcpkg)
- [Git](https://git-scm.com/downloads)
2) open the cmd app and run: `git clone https://github.com/MinetestClients/OpenClient.git`
3) run: `cd OpenClient`
4) run: `powershell`
5) now that you're in powershell, run: `vcpkg install irrlicht zlib curl[winssl] openal-soft libvorbis libogg sqlite3 freetype luajit --triplet x64-windows`
6) (still in powershell) run: `cmake . -G"Visual Studio 15 2017 Win64" -DCMAKE_TOOLCHAIN_FILE=D:/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_GETTEXT=0 -DENABLE_CURSES=0`
7) (still in powershell): `cmake --build . --config Release`
8) you'll find the executable in the current directory

# for the truly paranoid:
You can also recreate OpenClient. First, download minetest 5.0.1 from the official minetest github page (https://github.com/minetest/minetest/releases). Next, make the changes listed above (https://github.com/MinetestClients/OpenClient/blob/master/README.md#features-so-far-v1). Then run through the appropriate compiling method.

# contributors so far (along with their choice of getting advertised) (order on list only refers to when they started contributing):
username/nickname/name  | advertisement
------------- | -------------
sumoomus  | n/a
dankmemer  | https://www.youtube.com/channel/UCEWhEMACf_m2ffKsEeCEg0w
SPNG | n/a
Ryu | n/a
     
\*side note: contributors are not added according to their views or actions, only by their contributions to the project. The views/actions of the contributors (other than sumoomus) do not necessarily reflect the goal of OpenClient.

# Important:
OpenClient is not a hack client nor a cheat client, rather it is a Client Utility Mod (CUM). Please bear in mind that client utility mods like OpenClient can be against the rules on some servers.     
😉

### ps: star this repo if you found it useful so more ppl can find it, thanks!

## [NOT IMPLEMENTED YET] future features/functions that we're working on (if you can help out with any of these, msg me on Discord: algorithm#6184):
- user-friendly gui with keybind-setting to toggle on/off various functions     
Combat:
- Bow Aimbot
- Auto Armor
- Auto Clicker
- Auto Weapon
- Hunt
- Kill Aura
 
Movement:
- Air Jump
- Auto Jump
- Auto Walk (with some sort of pathfinding algorithm)
- Fast Fall
- Jesus (walk on water)
- Long Jump
- No Slow (dont get slowed down by soul sand, cobwebs, etc)
- Riding (control riding animals even when you dont have a saddle, entity speed)
- Safe Walk (dont fall off edges)
- Spider (climb walls)
- Sprint (enable sprinting)
- Step (control how far up can entities climb/step)
 
Exploit
- Anti Fire
- Anti Hunger
- New Chunks (find recently generated chunks)
 
Player
- Anti AFK
- Auto Eat
- Auto Farm
- Auto Fish
- Auto Mine
- Auto Tool
- Blink (give the illusion that you're teleporting by not sending packets to server for a few seconds)
- Chest Stealer
- Fast Interact (break and place quickly)
- Freecam (different from noclip)
- Inventory (see your inventory without having to press i or whatever key you use)
- Scaffold (place blocks underneath you)
 
Render
- Anti Blind (removes blindness potion effects)
- Breadcrumbs (plots a path of breadcrumbs behind you)
- ESP (show where players are even when you can't see them)
- Nametags (show nametags of players far away as well as their armor (+durability/enchants))
- Radar (sorta like enhanced minimap)
- Search (search for a specific node)
- Storage ESP (look for storage blocks)
- Tracers (draw lines to certain entities and/or players)
- Trajectories (predict trajectories of projectiles such as arrows and ender pearls)
- XRay
 
Misc
- Anti Vanish (detects when someone goes into vanish)
- Auto Disconnect (disconnect automatically when your health reaches a certain level)
- Auto Reconnect
- Fancy Chat
- copy paste things into chat
- book bot (automatically write books)
- Log Position (if a player logged out in your render distance, show their log-out coordinates)
- Signs (auto-sign signs)
- Unpack (create planks from logs by right clicking on the logs)
- autonoteblock (automatically tune and play songs using noteblocks and an input midi file (or something like that))
 
World
- Build (automatically build a certain structure)
- Weather (clear the weather)
